package net.iescierva.dam20_15.mislugareskotlin2021

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val texto = TextView(this)
        texto.text="Hola, Android"
        setContentView(texto)
    }

}