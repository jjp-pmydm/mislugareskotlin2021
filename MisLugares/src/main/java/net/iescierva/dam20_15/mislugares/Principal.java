package net.iescierva.dam20_15.mislugares;

public class Principal
{public static void main(String[] main) {
    GeoPunto iesCierva = new GeoPunto(37.9613893,-1.1361636);
    GeoPunto nuevaCondomina = new GeoPunto(38.0403299,-1.1485677);
    System.out.println("Distance: " + iesCierva.distancia(nuevaCondomina));

    Lugar lugar = new Lugar(
            "Escuela Politécnica Superior de Gandía",
            "C/ Paranimf, 1 46730 Gandia (SPAIN)",
            new GeoPunto(38.995656, -0.166093),
            "",
            TipoLugar.OTROS,
            962849300,
            "http://www.epsg.upv.es",
            "Uno de los mejores lugares para formarse.",
            3);
    System.out.println("Lugar " + lugar.toString());
   }
}
